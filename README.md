# Curium One 

The Curium Sat project is about the development of an open-source amateur radio experimentation CubeSat, aiming to provide global space-based communication for the amateur radio community. The satellite has been in development from July 2023 onwards by a group formed by students from the [Technical University of Berlin](https://www.tu.berlin/en/) and radio amateurs at [Libre Space Foundation](https://libre.space). ESA and [Planetary Transportation Systems GmbH](https://www.pts.space/) funded the launch and contributes to the open-source development. Almost all subsystem hardware designs and software have been made available publicly with open-source licenses. A launch opportunity with [Ariane 6](https://www.esa.int/Enabling_Support/Space_Transportation/Launch_vehicles/Ariane_6) maiden flight was used to bring Curium into orbit.

The integrated satellite before final integration after antenna trimming:

![image](6D044306-8CF4-49CC-A9A3-5AB7F47E68FB_1_105_c.jpeg){width=30%}


# Mission Goals

## Radio Amateur Experiments & Education
The Satellites secondary mission goal is to demonstrate the a near global communication link based entirely on open-source Amateur radio Hard and Software. To achieve this Curium provides a **store and forward functionality** for radio amateurs based on a message queue carrying source callsign, destination callsign and a message of up to 128 bytes as well as a time to live. The satellite is programmed to periodically retransmit up to 10 messages for up to one day each, chosen based on round-robing scheduling. 

A [public readme](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-ctrl/-/tree/curium-1/contrib/missions/curium-1) shows instructions on usage and provide documentation of the system. Furthermore a dashboard is in development that shows the number of interactions as well as other telemetry data of interest and shall enhance public visibility of the mission and the amateur radio community. This will be published on the [https://dashboard.satnogs.org](SatNOGS dashboard) section.

With the open-sourced hard and software, students and radio amateurs are actively organising and participating in the development of the Satellite serving various educational purposes. To support this, regular community events / presentations have been and will be performed locally.

## Open-Source development
The project shall contribute through open-source hard and software to satellite and amateur radio communities.
Most components of the Curium as well as their software are open-sourced, including:
- **On Board Computer (OBC)**: Managing operations in space
- **Electrical Power System (EPS)**: Handles power generation, storage and distribution
- **Battery System (BAT)**: Structural and electrical 4s2p Li-ION battery 
- **Attitude determination and control system (ADCS)**: Allows determination and control of the satelite's attitude
- **Communication System (SatNOGS-COMMS)**: [Libre Space SatNOGS-COMMS][(https://gitlab.com/librespacefoundation/satnogs-comms](https://docs.google.com/document/d/1QOcbxB9N_Mb0yUyeSQQ9H0zeVXtukuSRbpYqxWod0XE/edit#heading%253Dh.44ndaxshcuuf)) open-source UHF and S-Band capable Communication subsystem with compatability to the [SatNOGS Network](https://network.satnogs.org/) 

The open-source availability allows other amateur radio and space enthusiasts to learn and reuse the designs for their own goals! The repositories displayed here are regularly updated mirrors of private development repositories. Please do not hesitate to contact us at the [SatNOGS-COMMS matrix channel](https://app.element.io/#/room/#satnogs-comms:matrix.org).

## In-Orbit demonstration of SatNOGS-COMMS
As the primary mission goal the in-orbit demonstration of SatNOGS-COMMS as well as the all other subsystems mentioned above allow other entities of the community to draw on the generated data and validated designs. The SatNOGS-COMMS is a state-of-the-art open-source dual-band radio transceiver designed specifically for Telemetry and Telecommand (TMTC) operations within the S-band and UHF frequencies. The software-configurable radio module is optimized to provide robust communication channels for satellite operations. It supports in-flight reconfiguration capabilities including adjustments to carrier and intermediate frequencies, bitrate, modulation options, and channel-filter bandwidth allowing for many experimental and educational setups. SatNOGS-COMMS achieved TRL8 in 2023 and will fly its first time in UHF only configuration on Curium.


# Technical Specifications

### Structure:
- Size: 12U CubeSat
- Bus weight: ~5.6 Kg
- Material: Aluminium 7075, 5-axis milled

### Electrical Power System (EPS)
- Solar cells: Monocrystalline Si cells (Anysolar Ixsolar based)
- Power budget: depending on orbit, 2-5 W average for payload, higher burst loads >50W possible
- Energy storage: ~100Wh Lithium-Ion LG MJ1 cells 4S2P pack with temperature measurement and thermal insulation

Battery package made of FR-4 material:

![battery image](image.png){width=25%}

Solar panel illumiated by the sun:

![solar panels image](9CB551DD-F95E-4085-9282-AA8B4F420841_1_105_c.jpeg){width=25%}

EPS structure: 

![eps structure](image-2.png){width=35%}

### Attitude Determination & Control System (ADCS)
- Magnetorquer inside solar panels 
- Possibility to integratereaction wheels

### Communications Subsystem (SatNOGS-COMMS)
- Utilization of Satnogs-Comms Board: [link](https://libre.space/projects/satnogs-comms/)
- Amateur radio bands for uplink and downlink, with a data transfer rate of up to 50 kbps (UHF) and possible use of S-Band 
- AT86RF215 dual-band transceiver with I/Q Radio capabilities, Half-duplex operation
- Frequency step size: 100Hz in UHF
- Receiver Noise Figure: 1.5dB
- Framing encapsulation: CCSDS
- Data rates: up to 50kbps
- GMSK/GFSK,BPSK,QPSK modulation possible
- Antenna: four monopole antennas with radiation patterns to achieve quasi-isotropic radiation properties in turnstile configuration

SatNOGS COMMS FM after reflow soldering:

![satnogs image](image-1.png){width=25%}

### On-Board Computer (OBC)
- CPU: STM32H7 based as [rad testing](https://www.opensourcesatellite.org/microprocessor-radiation-test-results-stm32/) indicates good long-term performance
- Redundant 32M-Bit Serial Flash Memory
OBC section of Main PCB:

![OBC image](5334E71B-FF01-4A35-A6A1-84F1A2332E8C_1_105_c.jpeg){width=25%}

### Launch & Lifetime
- Launch vehicle compatability: Designed along [Ariane 6 user's manual](https://www.arianespace.com/wp-content/uploads/2021/03/Mua-6_Issue-2_Revision-0_March-2021.pdf)
- Lifetime: Designed for >3 months. Critical subsystems and components were carefully chosen and have previously shown 10+ years orbital lifetimes.


# Amateur Radio payload
The SatNOGS-COMMS onboard Curium-1 implements a amateur radio frame forwarder.
The forwarder is a message queue containing a maximum of 256 messages.
Each message carries a source and destination callsign, a message up to 128 bytes and a TTL (Time to Live) field.
The TTL is the time in seconds that the message should live in the queue before a periodic task of the satellite deletes it.
The maximum allowed TTL is one day.
In case the queue is full, and a new valid frame is received, the oldest message is deleted.
To avoid abuse of the forwarder, there is a frame limit of 1 frame per 5 seconds.
Frames that are sent faster than this limit are discarded.
The satellite periodically selects up to 10 messages in a round-robin way and broadcasts them.

SatNOGS-COMMS uses [Protocol Buffers](https://protobuf.dev/), for the frame contents definition, serialization and deserialization.
The protobuf definition for the amateur radio payload can be found at [hamradio.proto](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/blob/1e0f35cbb899eb111190887f428a62ad0c70b187/contrib/missions/curium-1/hamradio.proto) file.

Two different types of messages are defined, one for the uplink and one for the downlink.

```proto
message hamradio_uplink {
  bytes src_callsign = 1;
  bytes dst_callsign = 2;
  bytes payload = 3;
  uint32 ttl_secs = 4;
}

message hamradio_downlink {
  bytes src_callsign = 1;
  bytes dst_callsign = 2;
  bytes payload = 3;
}
```
More details for the amateur radio payload and its implementation, can be found at the source code of the mission, which is tracked under the [curium-1](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/tree/curium-1) branch of the [SatNOGS-COMMS software](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu) repository.
The [hamradio](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/blob/curium-1/src/hamradio.cpp?ref_type=heads) and [telemetry](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu/-/blob/curium-1/src/telemetry.cpp?ref_type=heads) classes implement the majority of the functionality.

## Modulation and framing
The Curium-1 uses MSK for both uplink and downlink with a baudrate of 50e3 symbols/sec.
The center frequency for both uplink and downlink is `436.240` MHz.
All frames are fixed in size, with 256 bytes for uplink and 452 for the downlink, including a 4-byte CRC32.
Zero padding is applied in case the information is less than the fixed size frame.

To determine the exact frame size, [length-delimited](https://protobuf.dev/programming-guides/encoding/#length-types) protobuf messages are used.
In this scheme, each protobuf message is prepented with a [varint](https://protobuf.dev/programming-guides/encoding/#varints), indicating the actual frame size.

Preamble, SYNC word and CRC fields are processed in a MSB first way and all fields are MSB first sent.

The uplink frame is constucted as:
|  Preamble | Sync word  | Payload  | CRC  |
|---|---|---|---|
|  `[0x55] x12` | `[0x1A, 0xCF, 0xFC, 0x1D]`   | 252 (fixed size)  |  `CRC32-C` (4 bytes) |

Similarly the downlink frame:
|  Preamble | Sync word  | Payload  | CRC  |
|---|---|---|---|
|  `[0x55] x12` | `[0x1A, 0xCF, 0xFC, 0x1D]`   | 448 (fixed size)  |  `CRC32-C` (4 bytes) |

The payload and CRC fields of the **downlink frames** are scrambled before tranmsission, using the CCSDS whitening algorithm.
The uplink frames are intensionally left without scrambling to simplify the communication with the satellite for less experienced operators.

Find out more about how to send and receive data here: https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-ctrl/-/blob/curium-1/contrib/missions/curium-1/README.md
